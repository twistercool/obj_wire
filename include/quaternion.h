#ifndef __OBJ_WIRE_QUATERNION_H_
#define __OBJ_WIRE_QUATERNION_H_

#include <math.h>

#include "struct.h"



struct quaternion quat_scale ( struct quaternion q, int scalar );
struct quaternion quat_dot ( struct quaternion q_0, struct quaternion q_1 );
struct quaternion quat_mul ( struct quaternion q0, struct quaternion q1 );
struct quaternion quat_conjugate(struct quaternion input);
struct quaternion quat_inverse(struct quaternion input);
struct quaternion quat_from_angle_axis(struct angle_axis_rot in);
struct quaternion quat_normalise(struct quaternion q);
struct vertex quat_rotate_vertex ( struct vertex v, struct quaternion q );



#endif
