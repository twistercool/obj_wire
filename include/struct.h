#ifndef __OBJ_WIRE_STRUCT_H_
#define __OBJ_WIRE_STRUCT_H_

#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

struct quaternion {
   double a;
   double b; /*times i*/
   double c; /*times j*/
   double d; /*times k*/
};

struct vertex {
   double x;
   double y;
   double z;
};

struct point {
   double x;
   double y;
};

struct point_i16 {
   int16_t x;
   int16_t y;
};

struct face {
   struct vertex v0;
   struct vertex v1;
   struct vertex v2;
};

struct angle_axis_rot {
   double x;
   double y;
   double z;
   double theta;
};

struct camera {
   struct vertex pos;
   struct quaternion q;
};

struct screen {
   uint32_t *rgba_arr;
   double *z_buff;
   size_t width;
   size_t height;   
};

struct rgba_u8 {
   uint8_t r;
   uint8_t g;
   uint8_t b;
   uint8_t a;
};

struct rgba_f64 {
   double r;
   double g;
   double b;
   double a;
};

struct vertex sum_vertices ( struct vertex v_0, struct vertex v_1 );
struct vertex sub_vertices ( struct vertex v_0, struct vertex v_1 );

uint32_t rgba_u8_to_u32 ( struct rgba_u8 in );

struct rgba_u8 mix_rgba_u8 ( struct rgba_u8 col_0, struct rgba_u8 col_1, double prop_col_0 );

void sort_face_y(struct face *f);

void fill_bottom_flat_triangle(struct face f, struct rgba_u8 col_0, struct rgba_u8 col_1, struct rgba_u8 col_2, struct screen *screen );
void fill_top_flat_triangle(struct face f, struct rgba_u8 col_0, struct rgba_u8 col_1, struct rgba_u8 col_2, struct screen *screen );

void draw_scanline ( int16_t x0, int16_t x1, int16_t y, int16_t z, struct rgba_u8 col_0, struct rgba_u8 col_1, struct screen *screen );
void draw_face(struct face f, struct rgba_u8 col_0, struct rgba_u8 col_1, struct rgba_u8 col_2, struct screen *screen);

#endif
