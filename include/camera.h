#ifndef __OBJ_WIRE_CAMERA_H_
#define __OBJ_WIRE_CAMERA_H_

#include <math.h>

#include "struct.h"
#include "quaternion.h"
   
/* all rotations in radian */
struct camera cam_rotate_horizontal ( struct camera cam, double dh );
struct camera cam_rotate_vertical ( struct camera cam, double dv );
struct camera cam_rotate_theta ( struct camera cam, double dt );

struct camera cam_move_horizontal ( struct camera cam, double dh );
struct camera cam_move_vertical ( struct camera cam, double dv );
struct camera cam_move_depth ( struct camera cam, double dd );

struct vertex camera_to_screen_space ( struct vertex v, double near );

#endif
