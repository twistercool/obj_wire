#ifndef __OBJ_WIRE_H_
#define __OBJ_WIRE_H_

#include <pierre.h>
#include <stdio.h>
#include <math.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdbool.h>
#include <assert.h>

#include "struct.h"
#include "quaternion.h"
#include "camera.h"

#define NEAR 1.0
#define APERTURE_WIDTH 3.5
#define APERTURE_HEIGHT 2.6
#define FOCAL_LENGTH 2


struct angle_axis_rot get_angle_axis_rot_from_quaternion(struct quaternion in);
double **get_transform_matrix(struct quaternion in);
void free_transform_matrix(double** matrix);
struct vertex transform_vert_mat(struct vertex in_v, struct quaternion in);



void handle_inputs(bool *run);
void update_display();

void run(struct face *faces, size_t nb_faces, struct vertex *vertices, size_t nb_vertices);


#endif
