VERSION = 0.1
INCS += -Iinclude
LIBS += -lm -lSDL2 -lSDL2main -lgsl -lgslcblas -lSDL2_ttf
CFLAGS += -std=gnu17 ${INCS} -march=native -Ofast -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable
DEBUG_CFLAGS += -std=gnu17 ${INCS} -Wall -Wextra -Wno-unused-parameter -Wno-unused-variable -Wno-unused-but-set-variable -DDEBUG -Og -g3 -ggdb
LDFLAGS += ${LIBS} -flto
DEBUG_LDFLAGS += ${LIBS}
TEST_LDFLAGS += ${DEBUG_LDFLAGS} -lcriterion
CC ?= gcc
STRIP ?= strip

SRC = $(wildcard src/*.c)
release_OBJ = $(patsubst src/%.c, target/release/%.o, ${SRC})
debug_OBJ = $(patsubst src/%.c, target/debug/%.o, ${SRC})
SRC_TEST = $(wildcard tests/*.c)
OBJ_TEST = ${SRC_TEST:.c=.o}

target/release/%.o: src/%.c
	@mkdir -p target/release
	${CC} ${CFLAGS} -c $< -o $@

target/release/obj_wire: ${release_OBJ}
	${CC} $^ ${CFLAGS} ${LDFLAGS} -o $@
	${STRIP} $@

target/debug/%.o: src/%.c
	@mkdir -p target/debug
	${CC} ${DEBUG_CFLAGS} -c $< -o $@

target/debug/obj_wire: ${debug_OBJ}
	${CC} $^ ${DEBUG_CFLAGS} ${DEBUG_LDFLAGS} -o $@

tests/test: $(filter-out %/main.o, ${debug_OBJ}) ${SRC_TEST}
	${CC} $^ ${DEBUG_CFLAGS} ${TEST_LDFLAGS} -o tests/test
	@echo -e '--RUNNING TESTS--'
	@./tests/test -j0 2>&1 | sed 's/^\[----\] .*:.*://g' | sed 's/^boxfort-worker: //g' || true
	@rm tests/test

clean:
	@echo -e '--CLEANING--'
	rm -f target/release/* target/debug/* ${OBJ_TEST}

all: target/release/obj_wire

release: target/release/obj_wire

debug: target/debug/obj_wire

test: tests/test

.PHONY: all release debug test clean
