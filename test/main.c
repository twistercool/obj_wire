#include "obj_wire.h"

void test_normal_scanline() {
   uint16_t width = 10;
   uint16_t height = 10;

   uint32_t pixels[width * height];
   double z_buff[width * height];

   memset(pixels, 0, sizeof(pixels));
   for (int i = 0; i < width * height; i++)
      z_buff[i] = DBL_MAX;


   struct rgba col = {255, 255, 255, 255};

   draw_scanline(4, 8, 3, 10, col, col, pixels, width, height, z_buff);

   assert(pixels[3 * width + 4] == UINT_MAX);
   assert(pixels[3 * width + 3] != UINT_MAX);
}

int main(int argc, char *argv[]) {
   test_normal_scanline();

   printf("All tests succeeded!\n");
}
