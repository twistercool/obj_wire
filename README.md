# obj\_wire
   This program means to visualise .obj files in 3D.

   It uses basic 3D rasterisation techniques to project the lines onto the screen without displaying the surfaces, for a retro look.


# Installation
   
   First, make sure you have installed SDL2 onto your computer.
  
   Then, navigate to the ./build directory and run the command:
   `make`

   Now, you can use the binary `obj_wire` in that directory to open any .obj file!

# Usage

`./obj_wire </path/to/obj/file>`
