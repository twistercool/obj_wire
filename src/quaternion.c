#include "quaternion.h"

inline
struct quaternion
quat_scale ( struct quaternion q, int scalar )
{
   return (struct quaternion) {
      .a = q.a * scalar,
      .b = q.b * scalar,
      .c = q.c * scalar,
      .d = q.d * scalar
   };   
}

inline
struct quaternion
quat_dot ( struct quaternion q0, struct quaternion q1 )
{
   return (struct quaternion) {
      .a = q0.a * q1.a,
      .b = q0.b * q1.b,
      .c = q0.c * q1.c,
      .d = q0.d * q1.d,
   };
}

inline
struct quaternion
quat_mul ( struct quaternion q0, struct quaternion q1 )
{
   return (struct quaternion) {
      .a = q0.a * q1.a - q0.b * q1.b - q0.c * q1.c - q0.d * q1.d,
      .b = q0.a * q1.b + q0.b * q1.a + q0.c * q1.d - q0.d * q1.c,
      .c = q0.a * q1.c + q0.c * q1.a + q0.d * q1.b - q0.b * q1.d,
      .d = q0.a * q1.d + q0.d * q1.a + q0.b * q1.c - q0.c * q1.b
   };
}

inline
struct quaternion
quat_conjugate(struct quaternion input)
{
   return (struct quaternion) {
      .a = input.a,
      .b = -input.b,
      .c = -input.c,
      .d = -input.d
   };
}


inline
struct quaternion
quat_inverse(struct quaternion in)
{
   double s = pow(sqrt(in.a * in.a + in.b * in.b + in.c * in.c + in.d * in.d), -2);
   return (struct quaternion) {
      .a = in.a * s,
      .b = -in.b * s,
      .c = -in.c * s,
      .d = -in.d * s
   };
}

inline
struct quaternion
quat_from_angle_axis(struct angle_axis_rot in)
{
   double sin_t_o_2 = sin(in.theta / 2);
   double sin_norm = sin_t_o_2 / sqrt(in.x * in.x + in.y * in.y + in.z * in.z);
   return (struct quaternion) {
      .a = cos(in.theta / 2),
      .b = in.x * sin_t_o_2,
      .c = in.y * sin_t_o_2,
      .d = in.z * sin_t_o_2
   };
}

inline
struct quaternion
quat_normalise(struct quaternion q)
{
   double magnitude = sqrt(q.a * q.a + q.b * q.b + q.c * q.c + q.d * q.d);
   return (struct quaternion) {
      .a = q.a / magnitude,
      .b = q.b / magnitude,
      .c = q.c / magnitude,
      .d = q.d / magnitude
   };
}

inline
struct vertex
quat_rotate_vertex ( struct vertex v, struct quaternion q )
{
   /* t = 2q x v */
   double tx = 2 * (q.c * v.z - q.d * v.y);
   double ty = 2 * (q.d * v.x - q.b * v.z);
   double tz = 2 * (q.b * v.y - q.c * v.x);
   /* v + w y + q x t */
   return (struct vertex) {
      v.x + q.a * tx + q.c * tz - q.d * ty,
      v.y + q.a * ty + q.d * tx - q.b * tz,
      v.z + q.a * tz + q.b * ty - q.c * tx
   };
}
