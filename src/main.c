#include "obj_wire.h"

int main(int argc, char* argv[]) {
   if (argc != 2) {
      fprintf(stderr, "Usage: obj_wire </path/to/file.obj>\n");
      return 0;
   }

   // load the specified .obj in argv[1]
   FILE* file = fopen(argv[1], "r");
   if (!file) {
      fprintf(stderr, "Error reading file\n");
      return 1;
   }

   // count vertices and lines in 3d in the file
   size_t nb_vertices = 0;
   size_t nb_faces = 0;
   int nb_lines_3d = 0;
   char line[364];
   while (fgets(line, 3, file)) {
      if (line[0] == 'v' && (strlen(line) == 1 || line[1] == ' '))
         nb_vertices++;
      else if (line[0] == 'f') {
         nb_faces++;
      }
   }

   // reloads the file
   fclose(file);
   file = fopen(argv[1], "r");
   if (!file) {
      fprintf(stderr, "Error reading file\n");
      return 1;
   }

   // loads all vertices/faces into memory
   struct vertex* vertices = malloc(sizeof(struct vertex) * nb_vertices);
   struct face* faces = malloc(sizeof(struct face) * nb_faces);

   int vertex_index = 0;
   while (fgets(line, sizeof(line) - 1, file)) {
      if (line[0] == 'v' && (strlen(line) == 1 || line[1] == ' ')) {
         // struct vertex found
         double x, y, z;
         sscanf(line, "v %lf %lf %lf", &x, &y, &z);
         vertices[vertex_index] = (struct vertex){x, y, z};
         vertex_index++;
      }
   }

   // reloads it again
   fclose(file);
   file = fopen(argv[1], "r");
   if (!file) {
      fprintf(stderr, "Error reading file\n");
      return 1;
   }

   int face_index = 0;
   while (fgets(line, sizeof(line) - 1, file)) {
      if (line[0] == 'f') {
         // face found
         char *token = strtok(line, " ");

         int v0 = -1, v1 = -1, v2 = -1;
         int nb_toks = 0;
         for (; nb_toks <= 3 && token != NULL; token = strtok(NULL, " "), nb_toks++) {
            if (nb_toks == 1)
               v0 = atoi(token);
            else if (nb_toks == 2)
               v1 = atoi(token);
            else if (nb_toks == 3)
               v2 = atoi(token);
         }

         if (nb_toks <= 3 || v0 == -1 || v1 == -1 || v2 == -1) {
            continue;
         }

         faces[face_index] =
             (struct face){vertices[v0 - 1], vertices[v1 - 1], vertices[v2 - 1]};
         face_index++;
      }
   }
   fclose(file);

   printf("Loading complete\n");

   run(faces, nb_faces, vertices, nb_vertices);
}
