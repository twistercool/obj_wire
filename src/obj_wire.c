#include "obj_wire.h"

size_t nb_faces, nb_vertices;
struct face *faces;
struct vertex *vertices;

struct camera cam = {
   .pos = {
      .x = 0,
      .y = 0,
      .z = 0
   },
   .q = {
      .a = 1,
      .b = 0,
      .c = 0,
      .d = 0
   }
};

struct screen g_screen = {0};

TTF_Font *g_font;

uint64_t last_ticks;

bool debug_info = true;


void
debug_print_camera_pos () {
   printf("Camera pos: { x: %f, y: %f, z: %f }\n", cam.pos.x, cam.pos.y, cam.pos.z);
   printf("Camera quat: { a: %f, bi: %f, cj: %f, dk: %f }\n\n", cam.q.a, cam.q.b, cam.q.c, cam.q.d);
}

inline
struct angle_axis_rot
get_angle_axis_rot_from_quaternion ( struct quaternion in )
{
   double eu_norm = sqrt(in.b * in.b + in.c * in.c + in.d * in.d);
   return (struct angle_axis_rot) {
      .x = in.b / eu_norm,
      .y = in.c / eu_norm,
      .z = in.d / eu_norm,
      .theta = 2 * atan2(eu_norm, in.a)
   };
}

/* struct OBJ_group { */
   /*A group has points, lines and faces (and maybe free-form geometry in the future)*/
/* } */

/* struct OBJ_state { */
   /*for each group, have a */
/* }; */

/* int */
/* read_file ( const char *filename, struct  ) { */
/*    FILE* file = fopen(filename, "r"); */
/*    if (!file) { */
/*       fprintf(stderr, "Error reading file '%s'\n", filename); */
/*       return 1; */
/*    } */

/*    // count vertices and lines in 3d in the file */
/*    size_t nb_vertices = 0; */
/*    size_t nb_faces = 0; */
/*    int nb_lines_3d = 0; */
/*    char line[364]; */
/*    while (fgets(line, 3, file)) { */
/*       if (line[0] == 'v' && (strlen(line) == 1 || line[1] == ' ')) */
/*          nb_vertices++; */
/*       else if (line[0] == 'f') { */
/*          nb_faces++; */
/*       } */
/*    } */

/*    // reloads the file */
/*    fclose(file); */
/*    file = fopen(argv[1], "r"); */
/*    if (!file) { */
/*       fprintf(stderr, "Error reading file\n"); */
/*       return 1; */
/*    } */

/*    // loads all vertices/faces into memory */
/*    struct vertex* vertices = malloc(sizeof(struct vertex) * nb_vertices); */
/*    struct face* faces = malloc(sizeof(struct face) * nb_faces); */

/*    int vertex_index = 0; */
/*    while (fgets(line, sizeof(line) - 1, file)) { */
/*       if (line[0] == 'v' && (strlen(line) == 1 || line[1] == ' ')) { */
/*          // struct vertex found */
/*          double x, y, z; */
/*          sscanf(line, "v %lf %lf %lf", &x, &y, &z); */
/*          vertices[vertex_index] = (struct vertex){x, y, z}; */
/*          vertex_index++; */
/*       } */
/*    } */

/*    // reloads it again */
/*    fclose(file); */
/*    file = fopen(argv[1], "r"); */
/*    if (!file) { */
/*       fprintf(stderr, "Error reading file\n"); */
/*       return 1; */
/*    } */

/*    int face_index = 0; */
/*    while (fgets(line, sizeof(line) - 1, file)) { */
/*       if (line[0] == 'f') { */
/*          // face found */
/*          char *token = strtok(line, " "); */

/*          int v0 = -1, v1 = -1, v2 = -1; */
/*          int nb_toks = 0; */
/*          for (; nb_toks <= 3 && token != NULL; token = strtok(NULL, " "), nb_toks++) { */
/*             if (nb_toks == 1) */
/*                v0 = atoi(token); */
/*             else if (nb_toks == 2) */
/*                v1 = atoi(token); */
/*             else if (nb_toks == 3) */
/*                v2 = atoi(token); */
/*          } */

/*          if (nb_toks <= 3 || v0 == -1 || v1 == -1 || v2 == -1) { */
/*             continue; */
/*          } */

/*          faces[face_index] = */
/*              (struct face){vertices[v0 - 1], vertices[v1 - 1], vertices[v2 - 1]}; */
/*          face_index++; */
/*       } */
/*    } */
/*    fclose(file); */
   
/* } */

// returns a pointer to a 2-d matrix
double **
get_transform_matrix(struct quaternion in)
{
   double **ret = malloc(sizeof(double *) * 3);
   for (int i = 0; i < 3; i++)
      ret[i] = malloc(sizeof(double) * 3);

   double s = pow(sqrt(in.a * in.a + in.b * in.b + in.c * in.c + in.d * in.d), -2);

   ret[0][0] = 1 - 2 * s * (in.c * in.c + in.d * in.d);
   ret[0][1] = 2 * s * (in.b * in.c - in.d * in.a);
   ret[0][2] = 2 * s * (in.b * in.d + in.c * in.a);

   ret[1][0] = 2 * s * (in.b * in.c + in.d * in.a);
   ret[1][1] = 1 - 2 * s * (in.b * in.b + in.d * in.d);
   ret[1][2] = 2 * s * (in.c * in.d - in.b * in.a);

   ret[2][0] = 2 * s * (in.b * in.d - in.c * in.a);
   ret[2][1] = 2 * s * (in.c * in.d + in.b * in.a);
   ret[2][2] = 1 - 2 * s * (in.b * in.b + in.c * in.c);

   return ret;
}

void
free_transform_matrix(double **matrix)
{
   for (int i = 0; i < 3; i++)
      free(matrix[i]);
   free(matrix);
}

struct vertex
transform_vert_mat (struct vertex in_v, struct quaternion in)
{
   struct vertex ret;
   double **m = get_transform_matrix(in);

   ret.x = m[0][0] * in_v.x + m[0][1] * in_v.y + m[0][2] * in_v.z;
   ret.y = m[1][0] * in_v.x + m[1][1] * in_v.y + m[1][2] * in_v.z;
   ret.z = m[2][0] * in_v.x + m[2][1] * in_v.y + m[2][2] * in_v.z;

   free_transform_matrix(m);

   return ret;
}



void
run ( struct face *in_faces, size_t in_nb_faces, struct vertex *in_vertices, size_t in_nb_vertices )
{
   faces = in_faces;
   nb_faces = in_nb_faces;
   vertices = in_vertices;
   nb_vertices = in_nb_vertices;

   if (SDL_Init(SDL_INIT_EVERYTHING)) {
      fprintf(stderr, "Init error: %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
   }

   if (TTF_Init() != 0) {
      fprintf(stderr, "TTF init error: %s\n", SDL_GetError());
      SDL_Quit();
      exit(EXIT_FAILURE);
   }

   if ((g_font = TTF_OpenFont("/usr/share/fonts/TTF/FiraCodeNerdFont-Regular.ttf", 24)) == NULL) {
      fprintf(stderr, "TTF init error: %s\n", SDL_GetError());
      TTF_Quit();
      SDL_Quit();
      exit(EXIT_FAILURE);
   }

   SDL_DisplayMode mode;
   SDL_GetCurrentDisplayMode(0, &mode);

   g_screen.width = mode.w;
   g_screen.height = mode.h;

   SDL_Window *win = SDL_CreateWindow("Obj_Wire", SDL_WINDOWPOS_UNDEFINED,
                                      SDL_WINDOWPOS_UNDEFINED, g_screen.width, g_screen.height,
                                      SDL_WINDOW_VULKAN);

   if (!win) {
      fprintf(stderr, "Problem initialising window, err %s\n", SDL_GetError());
      TTF_Quit();
      SDL_Quit();
      exit(EXIT_FAILURE);
   }

   SDL_Renderer *renderer = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

   if (!renderer) {
      fprintf(stderr, "Problem initialising renderer, err %s\n", SDL_GetError());
      TTF_Quit();
      SDL_Quit();
      exit(EXIT_FAILURE);
   }

   g_screen.rgba_arr = malloc(sizeof(Uint32) * g_screen.width * g_screen.height);
   g_screen.z_buff = malloc(sizeof(double) * g_screen.width * g_screen.height);
   for (size_t i = 0; i < g_screen.width * g_screen.height; i++)
      *(g_screen.z_buff + i) = DBL_MAX;

   SDL_Texture* screen_tex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR32, SDL_TEXTUREACCESS_STREAMING, g_screen.width, g_screen.height);

   bool run = true;

   while (run) {
      /* INPUTS */
      handle_inputs(&run);

      /* UPDATE */
      update_display();

      /* RENDER */

      SDL_RenderClear(renderer);

      void *pixels;
      int pitch;
      SDL_LockTexture(screen_tex, NULL, &pixels, &pitch);
      Uint32* dst = pixels;
      size_t x, y;
      for (y = 0; y < g_screen.height; y++)
         for (x = 0; x < g_screen.width; x++)
            *dst++ = *(g_screen.rgba_arr + y * g_screen.width + x);

      SDL_UnlockTexture(screen_tex);

      SDL_RenderCopy(renderer, screen_tex, NULL, NULL);

      /* renders debug info */
      if (debug_info) {
         /* Show fps counter */
         double fps = 1000.0 / (SDL_GetTicks64() - last_ticks);
         last_ticks = SDL_GetTicks64();

         char text_msg[32];
         snprintf(text_msg, 32, "FPS: %.2f", fps);

         SDL_Color text_colour = {255, 255, 255, 255};
         SDL_Surface *text_surface = TTF_RenderText_Solid(g_font, text_msg, text_colour);
         SDL_Texture *text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);

         SDL_Rect text_rect = {10, 10, 150, 80};
         SDL_RenderCopy(renderer, text_texture, NULL, &text_rect);
      }

      SDL_RenderPresent(renderer);
   }

   /* post-loop cleanup */
   free(g_screen.rgba_arr);
   free(g_screen.z_buff);
   free(vertices);
   free(faces);

   TTF_Quit();
   SDL_Quit();
}


void
handle_inputs ( bool *run )
{
   SDL_Event event;
   struct vertex dir_vect;
   struct angle_axis_rot rot;
   struct quaternion rot_quat;

   while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT) {
         *run = false;
         return;
      }
      else if (event.type == SDL_KEYDOWN) {
         switch (event.key.keysym.sym) {
            case SDLK_a:
               cam = cam_move_horizontal(cam, -1);
               break;
            case SDLK_d:
               cam = cam_move_horizontal(cam, 1);
               break;
            case SDLK_s:
               cam = cam_move_vertical(cam, -1);
               break;
            case SDLK_w:
               cam = cam_move_vertical(cam, 1);
               break;
            case SDLK_z:
               cam = cam_move_depth(cam, -1);
               break;
            case SDLK_x:
               cam = cam_move_depth(cam, 1);
               break;
            case SDLK_LEFT:
               cam = cam_rotate_horizontal(cam, -0.2);
               break;
            case SDLK_RIGHT:
               cam = cam_rotate_horizontal(cam, 0.2);
               break;
            case SDLK_UP:
               cam = cam_rotate_vertical(cam, -0.2);
               break;
            case SDLK_DOWN:
               cam = cam_rotate_vertical(cam, 0.2);
               break;
            case SDLK_e:
               cam = cam_rotate_theta(cam, -0.2);
               break;
            case SDLK_q:
               cam = cam_rotate_theta(cam, 0.2);
               break;

            default:
               break;
         }
         cam.q = quat_normalise(cam.q);
      }
   }
}

void
update_display()
{
   /* local variables */
   struct face cur_face, raster_f;
   struct vertex trans_v0, trans_v1, trans_v2, screen_v0, screen_v1, screen_v2, raster_v0, raster_v1, raster_v2;


   /* debug_print_camera_pos(); */

   /* left, right, bottom, top of screen */
   static double r = ((APERTURE_WIDTH / 2) / FOCAL_LENGTH) * NEAR;
   static double l = -((APERTURE_WIDTH / 2) / FOCAL_LENGTH) * NEAR;
   static double t = ((APERTURE_HEIGHT / 2) / FOCAL_LENGTH) * NEAR;
   static double b = -((APERTURE_HEIGHT / 2) / FOCAL_LENGTH) * NEAR;

   /* clears rgba_arr */
   memset(g_screen.rgba_arr, 0, sizeof(uint32_t) * g_screen.width * g_screen.height);
   /* resets z buffer */
   for (size_t i = 0; i < g_screen.width * g_screen.height; i++)
      *(g_screen.z_buff + i) = DBL_MAX;


   // updates roation of camera
   for (size_t i = 0; i < nb_faces; i++) {
      // project face onto screen_tex
      cur_face = faces[i];

      // reposition object in camera space
      cur_face.v0 = sub_vertices(cur_face.v0, cam.pos);
      cur_face.v1 = sub_vertices(cur_face.v1, cam.pos);
      cur_face.v2 = sub_vertices(cur_face.v2, cam.pos);

      //rotates the camera position with quaternions
      trans_v0 = quat_rotate_vertex(cur_face.v0, cam.q);
      trans_v1 = quat_rotate_vertex(cur_face.v1, cam.q);
      trans_v2 = quat_rotate_vertex(cur_face.v2, cam.q);

      // converts from camera space to screen space
      screen_v0 = camera_to_screen_space(trans_v0, NEAR);
      screen_v1 = camera_to_screen_space(trans_v1, NEAR);
      screen_v2 = camera_to_screen_space(trans_v2, NEAR);

      // converts from screen space to NDC space (in [-1, 1])
      double p_NDC_x_0 = (2 * screen_v0.x) / ((r - l) - (r + l) / (r - l));
      double p_NDC_y_0 = (2 * screen_v0.y) / ((t - b) - (t + b) / (t - b));

      double p_NDC_x_1 = (2 * screen_v1.x) / ((r - l) - (r + l) / (r - l));
      double p_NDC_y_1 = (2 * screen_v1.y) / ((t - b) - (t + b) / (t - b));

      double p_NDC_x_2 = (2 * screen_v2.x) / ((r - l) - (r + l) / (r - l));
      double p_NDC_y_2 = (2 * screen_v2.y) / ((t - b) - (t + b) / (t - b));

      // converts from NDC space to raster space (~=pixels)
      raster_v0 = (struct vertex) {
         .x = ((p_NDC_x_0 + 1) / 2) * g_screen.width,
         .y = ((1 - p_NDC_y_0) / 2) * g_screen.height,
         .z = -trans_v0.z
      };
      raster_v1 = (struct vertex) {
         .x = ((p_NDC_x_1 + 1) / 2) * g_screen.width,
         .y = ((1 - p_NDC_y_1) / 2) * g_screen.height,
         .z = -trans_v1.z
      };
      raster_v2 = (struct vertex) {
         .x = ((p_NDC_x_2 + 1) / 2) * g_screen.width,
         .y = ((1 - p_NDC_y_2) / 2) * g_screen.height,
         .z = -trans_v2.z
      };

      raster_f = (struct face) {
         .v0 = raster_v0,
         .v1 = raster_v1,
         .v2 = raster_v2
      };

      double depth = raster_f.v0.z + raster_f.v1.z + raster_f.v2.z;

      struct rgba_u8 col_0 = {
         .r = (depth * 23),
         .g = (depth * 71),
         .b = (depth * 91),
         .a = 255
      };
      struct rgba_u8 col_1 = {
         .r = (depth * 11),
         .g = (depth * 41),
         .b = (depth * 37),
         .a = 255
      };
      struct rgba_u8 col_2 = {
         .r = (depth * 89),
         .g = (depth * 7),
         .b = (depth * 61),
         .a = 255
      };

      /*DEBUG*/
      col_0 = (struct rgba_u8) {100, 100, 100, 255};
      col_1 = (struct rgba_u8) {100, 100, 100, 255};
      col_2 = (struct rgba_u8) {100, 100, 100, 255};

      draw_face(raster_f, col_0, col_1, col_2, &g_screen);
   }
}
