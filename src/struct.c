#include "struct.h"

inline
struct vertex
sum_vertices ( struct vertex v_0, struct vertex v_1 )
{
   return (struct vertex) {
      .x = v_0.x + v_1.x,
      .y = v_0.y + v_1.y,
      .z = v_0.z + v_1.z
   };
}

inline
struct vertex
sub_vertices ( struct vertex v_0, struct vertex v_1 )
{
   return (struct vertex) {
      .x = v_0.x - v_1.x,
      .y = v_0.y - v_1.y,
      .z = v_0.z - v_1.z
   };
}

uint32_t
rgba_u8_to_u32 ( struct rgba_u8 in )
{
   return (uint32_t) (in.r << 24 | in.g << 16 | in.b << 8 | in.a);
}

struct rgba_u8
mix_rgba_u8 ( struct rgba_u8 col_0, struct rgba_u8 col_1, double amnt_col_0 )
{
   if (amnt_col_0 < 0 || amnt_col_0 > 1) {
      fprintf(stderr, "Incorrect input amnt_col_0\n");
      return (struct rgba_u8) {0};
   }
   double amnt_col_1 = 1.0 - amnt_col_0;

   return (struct rgba_u8) {
      .r = (uint8_t) (col_0.r * amnt_col_0) + (uint8_t) (col_1.r * amnt_col_1),
      .g = (uint8_t) (col_0.g * amnt_col_0) + (uint8_t) (col_1.g * amnt_col_1),
      .b = (uint8_t) (col_0.b * amnt_col_0) + (uint8_t) (col_1.b * amnt_col_1),
      .a = (uint8_t) (col_0.a * amnt_col_0) + (uint8_t) (col_1.a * amnt_col_1)
   };
}

/*TODO: actually calculate z well instead of just approximating*/
inline
void
draw_scanline ( int16_t x0, int16_t x1, int16_t y, int16_t z, struct rgba_u8 col_0, struct rgba_u8 col_1, struct screen *screen )
{
   if (y < 0 || y >= (int16_t) screen->height)
      return;

   if (x0 > x1) {
      int16_t tmp = x0;
      x0 = x1;
      x1 = tmp;
   }

   x0 = (int16_t) fmax(x0, 0);
   x1 = (int16_t) fmax(x1, 0);
   x0 = (int16_t) fmin(x0, screen->width - 1);
   x1 = (int16_t) fmin(x1, screen->width - 1);

   if (x0 >= (int16_t) (screen->width - 1))
      return;
   if (x1 <= 0)
      return;
   if (x0 == x1) {
      /*TODO implement well*/
      return;
   }

   struct rgba_u8 cur_col;
   uint32_t *cur_ptr = screen->rgba_arr + y * screen->width + x0;
   double *z_ptr = screen->z_buff + y * screen->width + x0;
   for (int16_t x = x0; x <= x1; x++, z_ptr++, cur_ptr++) {
      if (*z_ptr > z) {
         cur_col = mix_rgba_u8(col_0, col_1, 1.0 * (x - x0) / (x1 - x0));
         *cur_ptr = rgba_u8_to_u32(cur_col);
         *z_ptr = z;
      }
   }
}

/*
   * sorts the face's vertices for ascending y
   * (f.v0 has the LOWEST y, HIGHEST on screen)
*/
inline
void
sort_face_y(struct face *f)
{
   struct vertex tmp;
   if (f->v0.y > f->v1.y) {
      tmp = f->v0;
      f->v0 = f->v1;
      f->v1 = tmp;
   }
   if (f->v1.y > f->v2.y) {
      tmp = f->v1;
      f->v1 = f->v2;
      f->v2 = tmp;
   }
   if (f->v0.y > f->v1.y) {
      tmp = f->v0;
      f->v0 = f->v1;
      f->v1 = tmp;
   }
}

/*
   * assumes sorted vertices in the face with y ascending
   * and f.v1.y == f.v2.y
*/
inline
void
fill_bottom_flat_triangle(struct face f, struct rgba_u8 col_0, struct rgba_u8 col_1, struct rgba_u8 col_2, struct screen *screen)
{
#ifdef DEBUG
   assert(f.v0.y <= f.v1.y + 0.01);
   assert(f.v0.y <= f.v2.y + 0.01);
   assert(f.v1.y <= f.v2.y + 0.01);
#endif

   double inv_sloap_0 = (f.v1.x - f.v0.x) / (f.v1.y - f.v0.y);
   double inv_sloap_1 = (f.v2.x - f.v0.x) / (f.v2.y - f.v0.y);

   double cur_x0 = f.v0.x;
   double cur_x1 = f.v0.x;

   struct rgba_f64 cur_col_0 = {
      .r = col_0.r,
      .g = col_0.g,
      .b = col_0.b,
      .a = col_0.a,
   };
   struct rgba_f64 cur_col_1 = {
      .r = col_0.r,
      .g = col_0.g,
      .b = col_0.b,
      .a = col_0.a,
   };


   struct rgba_f64 col_0_slope = {
      .r = ((double)col_1.r - (double)col_0.r) / (f.v1.y - f.v0.y),
      .g = ((double)col_1.g - (double)col_0.g) / (f.v1.y - f.v0.y),
      .b = ((double)col_1.b - (double)col_0.b) / (f.v1.y - f.v0.y),
      .a = ((double)col_1.a - (double)col_0.a) / (f.v1.y - f.v0.y),
   };

   struct rgba_f64 col_1_slope = {
      .r = ((double)col_2.r - (double)col_0.r) / (f.v2.y - f.v0.y),
      .g = ((double)col_2.g - (double)col_0.g) / (f.v2.y - f.v0.y),
      .b = ((double)col_2.b - (double)col_0.b) / (f.v2.y - f.v0.y),
      .a = ((double)col_2.a - (double)col_0.a) / (f.v2.y - f.v0.y),
   };

   for (int y = (int) f.v0.y; y < (int) f.v1.y; y++) {
      /*calculate Z*/
      struct rgba_u8 cur_col_0_u8 = {
         .r = cur_col_0.r,
         .g = cur_col_0.g,
         .b = cur_col_0.b,
         .a = cur_col_0.a,
      };
      struct rgba_u8 cur_col_1_u8 = {
         .r = cur_col_1.r,
         .g = cur_col_1.g,
         .b = cur_col_1.b,
         .a = cur_col_1.a,
      };
      draw_scanline((int16_t)cur_x0, (int16_t)cur_x1, (int16_t)y, (int16_t)f.v0.z, cur_col_0_u8, cur_col_1_u8, screen);
      cur_x0 += inv_sloap_0;
      cur_x1 += inv_sloap_1;
      cur_col_0.r += col_0_slope.r;
      cur_col_0.g += col_0_slope.g;
      cur_col_0.b += col_0_slope.b;
      cur_col_0.a += col_0_slope.a;
      cur_col_1.r += col_1_slope.r;
      cur_col_1.g += col_1_slope.g;
      cur_col_1.b += col_1_slope.b;
      cur_col_1.a += col_1_slope.a;
#ifdef DEBUG
   assert(cur_col_0.r >= fmin(col_0.r, col_1.r) && cur_col_0.r <= fmax(col_0.r, col_1.r));
   assert(cur_col_0.g >= fmin(col_0.g, col_1.g) && cur_col_0.g <= fmax(col_0.g, col_1.g));
   assert(cur_col_0.b >= fmin(col_0.b, col_1.b) && cur_col_0.b <= fmax(col_0.b, col_1.b));
   assert(cur_col_0.a >= fmin(col_0.a, col_1.a) && cur_col_0.a <= fmax(col_0.a, col_1.a));

   assert(cur_col_1.r >= fmin(col_0.r, col_2.r) && cur_col_1.r <= fmax(col_0.r, col_2.r));
   assert(cur_col_1.g >= fmin(col_0.g, col_2.g) && cur_col_1.g <= fmax(col_0.g, col_2.g));
   assert(cur_col_1.b >= fmin(col_0.b, col_2.b) && cur_col_1.b <= fmax(col_0.b, col_2.b));
   assert(cur_col_1.a >= fmin(col_0.a, col_2.a) && cur_col_1.a <= fmax(col_0.a, col_2.a));
#endif
   }
   /*this is needed to draw the last row of the bottom-flat triangle, or the top of a top-flat triangle*/
   if (f.v1.y - f.v0.y > 1.0)
      draw_scanline((int16_t)f.v1.x, (int16_t)f.v2.x, (int16_t)f.v1.y, (int16_t)f.v0.z, col_1, col_2, screen);
}

/*
   * assumes sorted vertices in the face with y ascending
   * and f.v0.y == f.v1.y
*/
inline
void
fill_top_flat_triangle(struct face f, struct rgba_u8 col_0, struct rgba_u8 col_1, struct rgba_u8 col_2, struct screen *screen)
{
#ifdef DEBUG
   assert(f.v0.y <= f.v1.y + 0.01);
   assert(f.v0.y <= f.v2.y + 0.01);
   assert(f.v1.y <= f.v2.y + 0.01);
#endif

   double inv_sloap_0 = (f.v2.x - f.v0.x) / (f.v2.y - f.v0.y);
   double inv_sloap_1 = (f.v2.x - f.v1.x) / (f.v2.y - f.v1.y);

   double cur_x0 = f.v2.x;
   double cur_x1 = f.v2.x;

   struct rgba_f64 cur_col_0 = {
      .r = col_2.r,
      .g = col_2.g,
      .b = col_2.b,
      .a = col_2.a,
   };
   struct rgba_f64 cur_col_1 = {
      .r = col_2.r,
      .g = col_2.g,
      .b = col_2.b,
      .a = col_2.a,
   };

   struct rgba_f64 col_0_slope = {
      .r = ((double)col_2.r - (double)col_0.r) / (f.v2.y - f.v0.y),
      .g = ((double)col_2.g - (double)col_0.g) / (f.v2.y - f.v0.y),
      .b = ((double)col_2.b - (double)col_0.b) / (f.v2.y - f.v0.y),
      .a = ((double)col_2.a - (double)col_0.a) / (f.v2.y - f.v0.y),
   };

   struct rgba_f64 col_1_slope = {
      .r = ((double)col_2.r - (double)col_1.r) / (f.v2.y - f.v1.y),
      .g = ((double)col_2.g - (double)col_1.g) / (f.v2.y - f.v1.y),
      .b = ((double)col_2.b - (double)col_1.b) / (f.v2.y - f.v1.y),
      .a = ((double)col_2.a - (double)col_1.a) / (f.v2.y - f.v1.y),
   };

   for (int y = (int) f.v2.y; y > (int) f.v0.y; y--) {
      /*TODO calculate Z*/
      struct rgba_u8 cur_col_0_u8 = {
         .r = cur_col_0.r,
         .g = cur_col_0.g,
         .b = cur_col_0.b,
         .a = cur_col_0.a,
      };
      struct rgba_u8 cur_col_1_u8 = {
         .r = cur_col_1.r,
         .g = cur_col_1.g,
         .b = cur_col_1.b,
         .a = cur_col_1.a,
      };
      draw_scanline((int16_t)cur_x0, (int16_t)cur_x1, (int16_t)y, (int16_t)f.v0.z/*tmp*/, cur_col_0_u8, cur_col_1_u8, screen);
      cur_x0 -= inv_sloap_0;
      cur_x1 -= inv_sloap_1;
      cur_col_0.r -= col_0_slope.r;
      cur_col_0.g -= col_0_slope.g;
      cur_col_0.b -= col_0_slope.b;
      cur_col_0.a -= col_0_slope.a;
      cur_col_1.r -= col_1_slope.r;
      cur_col_1.g -= col_1_slope.g;
      cur_col_1.b -= col_1_slope.b;
      cur_col_1.a -= col_1_slope.a;

#ifdef DEBUG
   assert(cur_col_0.r >= fmin(col_0.r, col_2.r) && cur_col_0.r <= fmax(col_0.r, col_2.r));
   assert(cur_col_0.g >= fmin(col_0.g, col_2.g) && cur_col_0.g <= fmax(col_0.g, col_2.g));
   assert(cur_col_0.b >= fmin(col_0.b, col_2.b) && cur_col_0.b <= fmax(col_0.b, col_2.b));
   assert(cur_col_0.a >= fmin(col_0.a, col_2.a) && cur_col_0.a <= fmax(col_0.a, col_2.a));

   assert(cur_col_1.r >= fmin(col_1.r, col_2.r) && cur_col_1.r <= fmax(col_1.r, col_2.r));
   assert(cur_col_1.g >= fmin(col_1.g, col_2.g) && cur_col_1.g <= fmax(col_1.g, col_2.g));
   assert(cur_col_1.b >= fmin(col_1.b, col_2.b) && cur_col_1.b <= fmax(col_1.b, col_2.b));
   assert(cur_col_1.a >= fmin(col_1.a, col_2.a) && cur_col_1.a <= fmax(col_1.a, col_2.a));
#endif
   }
}

/*
*/
void
draw_face(struct face f, struct rgba_u8 col_0, struct rgba_u8 col_1, struct rgba_u8 col_2, struct screen *screen)
{
   if (isnan(f.v0.x) || isnan(f.v1.x) || isnan(f.v2.x) ||
       isnan(f.v0.y) || isnan(f.v1.y) || isnan(f.v2.y) ||
       isnan(f.v0.x) || isnan(f.v1.x) || isnan(f.v2.x) ||
       isnan(f.v0.y) || isnan(f.v1.y) || isnan(f.v2.y))
      return;

   if (f.v0.z <= 0 || f.v1.z <= 0 || f.v2.z <= 0)
      return;

   int min_x = (int) fmin(f.v0.x, fmin(f.v1.x, f.v2.x));
   int max_x = (int) fmax(f.v0.x, fmax(f.v1.x, f.v2.x));
   if (max_x < 0 || min_x >= (int) screen->width)
      return;

   int min_y = (int) fmin(f.v0.y, fmin(f.v1.y, f.v2.y));
   int max_y = (int) fmax(f.v0.y, fmax(f.v1.y, f.v2.y));
   if (max_y < 0 || min_y >= (int) screen->height)
      return;

   sort_face_y(&f);

#ifdef DEBUG
   assert(f.v0.y <= f.v1.y + 0.01);
   assert(f.v0.y <= f.v2.y + 0.01);
   assert(f.v1.y <= f.v2.y + 0.01);
#endif

   if (f.v1.y == f.v2.y) {
      fill_bottom_flat_triangle(f, col_0, col_1, col_2, screen);
   }
   else if (f.v0.y == f.v1.y) {
      fill_top_flat_triangle(f, col_0, col_1, col_2, screen);
   }
   else {
      double ratio_d_v0v1_v0vv2_y = (f.v1.y - f.v0.y) / (f.v2.y - f.v0.y);
      struct vertex v3 = {
         .x = f.v0.x + ratio_d_v0v1_v0vv2_y * (f.v2.x - f.v0.x),
         .y = f.v1.y,
         .z = f.v0.z + ratio_d_v0v1_v0vv2_y * (f.v2.z - f.v0.z)
      };
      struct rgba_u8 col_3 = {
         .r = ((1 - ratio_d_v0v1_v0vv2_y) * (double)col_0.r + ratio_d_v0v1_v0vv2_y * (double)col_2.r),
         .g = ((1 - ratio_d_v0v1_v0vv2_y) * (double)col_0.g + ratio_d_v0v1_v0vv2_y * (double)col_2.g),
         .b = ((1 - ratio_d_v0v1_v0vv2_y) * (double)col_0.b + ratio_d_v0v1_v0vv2_y * (double)col_2.b),
         .a = ((1 - ratio_d_v0v1_v0vv2_y) * (double)col_0.a + ratio_d_v0v1_v0vv2_y * (double)col_2.a),
      };

      struct face bottom_flat_f = {f.v0, f.v1, v3};
      fill_bottom_flat_triangle(bottom_flat_f, col_0, col_1, col_3, screen);

      struct face top_flat_f = {f.v1, v3, f.v2};
      fill_top_flat_triangle(top_flat_f, col_1, col_3, col_2, screen);
   }
}
