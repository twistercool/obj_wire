#include "camera.h"

inline
struct camera
cam_rotate_horizontal ( struct camera cam, double dh )
{
   struct quaternion rot_quat = quat_from_angle_axis((struct angle_axis_rot) {0, dh, 0, fabs(dh)});
   cam.q = quat_mul(rot_quat, cam.q);
   return cam;
}

inline
struct camera
cam_rotate_vertical ( struct camera cam, double dv )
{
   struct quaternion rot_quat = quat_from_angle_axis((struct angle_axis_rot) {dv, 0, 0, fabs(dv)});
   cam.q = quat_mul(rot_quat, cam.q);
   return cam;
}

inline
struct camera
cam_rotate_theta ( struct camera cam, double dt )
{
   struct quaternion rot_quat = quat_from_angle_axis((struct angle_axis_rot) {0, 0, dt, fabs(dt)});
   cam.q = quat_mul(rot_quat, cam.q);
   return cam;
}

inline
struct camera
cam_move_horizontal ( struct camera cam, double dh )
{
   struct vertex dir_vect = {dh, 0, 0};
   dir_vect = quat_rotate_vertex(dir_vect, quat_inverse(cam.q));
   cam.pos = sum_vertices(cam.pos, dir_vect);
   return cam;
}

inline
struct camera
cam_move_vertical ( struct camera cam, double dv )
{
   struct vertex dir_vect = {0, dv, 0};
   dir_vect = quat_rotate_vertex(dir_vect, quat_inverse(cam.q));
   cam.pos = sum_vertices(cam.pos, dir_vect);
   return cam;
}

inline
struct camera
cam_move_depth ( struct camera cam, double dd )
{
   struct vertex dir_vect = {0, 0, dd};
   dir_vect = quat_rotate_vertex(dir_vect, quat_inverse(cam.q));
   cam.pos = sum_vertices(cam.pos, dir_vect);
   return cam;
}

inline
struct vertex
camera_to_screen_space ( struct vertex v, double near )
{
   return (struct vertex) {
      .x = near * v.x / (-v.z),
      .y = near * v.y / (-v.z),
      .z = -v.z
   };
}
